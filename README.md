# Composer template for BAH Drupal projects #

This BAH Drupal project template should provide a kickstart for managing your site dependencies with Composer.

## Usage

- git clone the repo
- Download and install composer. This will download and install Composer as a system-wide command named composer, under /usr/local/bin. 

```
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

```
- run "composer install" inside the web directory


## Do a site install with drush

Drush 8 is required.

```
drush site-install standard --account-name=admin --account-pass=admin --db-url=mysql://root:root@localhost/drupal

```

If you correctly set up the path in settings.php to the config folder, all settings will be imported during
the site install.

## What does the template do?

When installing the given composer.json some tasks are taken care of:

- Drupal will be installed in the web-directory.
- Autoloader is implemented to use the generated composer autoloader in vendor/autoload.php, instead of the one provided by Drupal (web/vendor/autoload.php).
- Modules (packages of type drupal-module) will be placed in web/modules/contrib/
- Theme (packages of type drupal-theme) will be placed in web/themes/contrib/
- Profiles (packages of type drupal-profile) will be placed in web/profiles/contrib/
- Creates default writable versions of settings.php and services.yml.
- Creates sites/default/files-directory.